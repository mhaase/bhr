use std::env;
use std::error::Error;
use std::fs::File;
use std::io::{BufRead, BufReader};

use sha1::{Digest, Sha1};

const EXPECTED_SHA1_HASH_LENGTH: usize = 40;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();

    let (wordlist, sha1_hash) = match args.len() {
        2 => ("files/wordlist.txt", args[1].as_str()),
        3 => (args[1].as_str(), args[2].as_str()),
        _ => {
            println!("Usage: sha1_c [wordlist] <sha1_hash>");
            return Err("Usage: sha1_c [wordlist] <sha1_hash>".into());
        }
    };

    if sha1_hash.len() != EXPECTED_SHA1_HASH_LENGTH {
        return Err("invalid hash provided (not a SHA1 hash?)".into());
    }

    let file = File::open(&wordlist)?;
    let file_reader = BufReader::new(file);

    for line in file_reader.lines() {
        let line = line?;
        let hash_of_line = hex::encode(Sha1::digest(line.as_bytes()));
        if hash_of_line.eq(&sha1_hash) {
            println!("found match: {}", &line);
            return Ok(());
        }
    }

    println!("could not find match for given hash");

    Ok(())
}
